#!/bin/bash

psw=`cat cred.txt`


#Copying the file
copy(){
        echo -e "===============================================\n\nNow We are copying the file to $line.\n\n==============================================="
        echo y | pscp -pw $psw ./nopasswd_ssh.sh itunix@$line:/home/itunix/
        sshpass -p $psw ssh -n -T itunix@$line "chmod ug+wx /home/itunix/nopasswd_ssh.sh"
        echo -e "===============================================\n\nCopying the file has been completed to $line.\n\n==============================================="
}

#verifying the copied file
verify(){
        verif=$( grep "%" ./verify_log.txt | awk '{print $12}' )
        if [[ $verif == "100%" ]]
        then echo -e "===============================================\n\nThe file has been copied to $line successfully.\n\n===============================================\n"
        else echo -e "===============================================\n\nThe file might've not been copied to $line successfully.\n\n===============================================\n"
        fi
        echo -e "===============================================\n Now We are verifying the file on $line.\n===============================================\n"
        sshpass -p $psw ssh -n -T itunix@$line "ls -lha /home/itunix/nopasswd_ssh.sh" 
        echo -e "\n===============================================\n Verification completed on $line.\n===============================================\n"
        sleep 1s
}


#Main

while read line
        do
        copy
        sleep 1s
        verify
done < servers.txt